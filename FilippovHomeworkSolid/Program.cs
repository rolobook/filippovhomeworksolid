﻿using Spectre.Console;

namespace FilippovHomeworkSolid
{
    internal class Program
    {
        enum ProgramChoice
        {
            Play,
            Settings,
            Exit
        }

        static void Main(string[] args)
        {
            var run = true;
            while (run)
            {
                var choice = AnsiConsole.Prompt(
                    new SelectionPrompt<ProgramChoice>()
                        .Title("")
                        .AddChoices(new[] {
                            ProgramChoice.Play,
                            ProgramChoice.Settings,
                            ProgramChoice.Exit,
                        })
                        .UseConverter((c) => c switch {
                            ProgramChoice.Play => "Играть",
                            ProgramChoice.Settings => "Настройки",
                            ProgramChoice.Exit => "Выход",
                            _ => "",
                        })
                );

                try
                {
                    switch (choice)
                    {
                        case ProgramChoice.Play:
                            ProgramPlay.Run(ProgramSettings.GetRandomGenerator());
                            break;
                        case ProgramChoice.Settings:
                            ProgramSettings.Run();
                            break;
                        case ProgramChoice.Exit:
                            run = false;
                            break;
                    }
                }
                catch (Exception ex)
                {
                    AnsiConsole.WriteLine(ex.Message);
                }
            }
        }
    }
}