﻿namespace FilippovHomeworkSolid
{
    internal interface ISeedableRandomGenerator
    {
        public void SetSeed(int val);
    }
}
