﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilippovHomeworkSolid
{
    internal interface IIntegerRandomGenerator
    {
        /// <summary>
        /// Возвращает случайное число от min до max включительно
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public int GetInt(int min, int max);
    }
}
