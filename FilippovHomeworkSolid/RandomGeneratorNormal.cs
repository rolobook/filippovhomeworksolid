﻿namespace FilippovHomeworkSolid
{
    internal class RandomGeneratorNormal : IIntegerRandomGenerator
    {
        public virtual int GetInt(int min, int max)
        {
            return Random.Shared.Next(min, max + 1);
        }
    }
}
