﻿using Spectre.Console;

namespace FilippovHomeworkSolid
{
    internal class ProgramPlay
    {
        public static void Run(IIntegerRandomGenerator randomGenerator)
        {
            ushort attempts = GameSettings.Default.Attempts;
            int answer = randomGenerator.GetInt(GameSettings.Default.Min, GameSettings.Default.Max);
            AnsiConsole.WriteLine("Загадано число от {0} до {1}", GameSettings.Default.Min, GameSettings.Default.Max);

            do
            {
                int guess = AnsiConsole.Ask<int>("Введите число:");

                if (guess < GameSettings.Default.Min || guess > GameSettings.Default.Max)
                {
                    AnsiConsole.WriteLine("Нужно ввести число от {0} до {1}", GameSettings.Default.Min, GameSettings.Default.Max);
                    continue;
                }

                if (answer > guess)
                {
                    AnsiConsole.MarkupLine("Загаданное число [bold]больше[/]");
                }
                else if (answer < guess)
                {
                    AnsiConsole.MarkupLine("Загаданное число [bold]меньше[/]");
                }
                else
                {
                    AnsiConsole.MarkupLine("[green]Вы угадали![/]");
                    return;
                }

                AnsiConsole.WriteLine("Осталось попыток: {0}", --attempts);

            } while (attempts > 0);

            AnsiConsole.MarkupLine("[red]Вы не угадали.[/]");
        }
    }
}
