﻿namespace FilippovHomeworkSolid
{
    internal class RandomGeneratorBetter : RandomGeneratorNormal
    {
        public override int GetInt(int min, int max)
        {
            return System.Security.Cryptography.RandomNumberGenerator.GetInt32(min, max + 1);
        }
    }
}
