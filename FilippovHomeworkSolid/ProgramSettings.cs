﻿using Spectre.Console;

namespace FilippovHomeworkSolid
{
    internal class ProgramSettings
    {
        public static void Run()
        {
            var choice = AnsiConsole.Prompt(
                   new SelectionPrompt<RNGType>()
                       .Title("Выберите тип генератора случайных чисел:")
                       .AddChoices(new[] {
                            RNGType.Normal,
                            RNGType.Better,
                            RNGType.Best,
                       })
                       .UseConverter((c) => c switch {
                           RNGType.Normal => "Обычный генератор",
                           RNGType.Better => "По-лучше",
                           RNGType.Best => "Лучший!",
                           _ => "",
                       })
               );

            GameSettings.Default.RNGType = (int)choice;

            while (true)
            {
                int min = AnsiConsole.Ask<int>("Минимальное число?");
                if (min > int.MaxValue - 1)
                {
                    AnsiConsole.WriteLine("Число должно быть от {0} до {1}", int.MinValue, int.MaxValue - 1);
                    continue;
                }

                GameSettings.Default.Min = min;
                break;
            }

            while (true)
            {
                int max = AnsiConsole.Ask<int>("Максимальное число?");
                if (GameSettings.Default.Min > max || max > int.MaxValue - 1)
                {
                    AnsiConsole.WriteLine("Число должно быть от {0} до {1}", GameSettings.Default.Min, int.MaxValue - 1);
                    continue;
                }

                GameSettings.Default.Max = max;
                break;
            }

            while (true)
            {
                int attempts = AnsiConsole.Ask<int>("Число попыток?");
                if (attempts < 1 || attempts > ushort.MaxValue)
                {
                    AnsiConsole.WriteLine("Число должно быть от {0} до {1}", 1, ushort.MaxValue);
                    continue;
                }

                GameSettings.Default.Attempts = (ushort)attempts;
                break;
            }
                
            GameSettings.Default.Save();
        }

        public static IIntegerRandomGenerator GetRandomGenerator()
        {
            return GameSettings.Default.RNGType switch {
                1 => new RandomGeneratorBetter(),
                2 => new RandomGeneratorBest(),
                _ => new RandomGeneratorNormal(),
            };
        }
    }

    internal enum RNGType : int
    {
        Normal,
        Better,
        Best
    }
}
